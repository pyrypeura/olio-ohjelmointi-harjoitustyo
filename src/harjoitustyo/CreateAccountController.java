/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyo;

import harjoitustyo.users.Users;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Veeti
 */
public class CreateAccountController implements Initializable {

    @FXML
    private TextField userName;
    @FXML
    private PasswordField password;
    @FXML
    private PasswordField check;
    @FXML
    private Label messageField;
    @FXML
    private Button create;
    
    private Users u = Users.getInstance();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void create(ActionEvent event) { //if passwords match program sends data to user class and creates new user
        if (!password.getText().equals(check.getText())){
            messageField.setText("Salasanat eivät täsmää");
        } else {
            String text = u.createUser(userName.getText(), password.getText());
            messageField.setText(text);
        }
    }
    
}
