/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyo.timo;

import java.util.ArrayList;

/**
 *
 * @author Pyry
 */
public class GeoPoint {
    private ArrayList<String> point = new ArrayList();
    
    public GeoPoint(String la, String lo){ //saves latitude and longitude of certain point
        point.add(la);
        point.add(lo);
    }
    
    public ArrayList<String> getList(){ //returns latitude and longitude of geopoint as an ArrayList
        return point;
    }
}
