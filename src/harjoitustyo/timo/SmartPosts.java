/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyo.timo;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import java.util.Collections;

/**
 *
 * @author Pyry
 */
public class SmartPosts {
    private static SmartPosts sp = null;
    private ArrayList<SmartPost> posts = new ArrayList();
    private ArrayList<String> cities = new ArrayList();
    
    private SmartPosts(){
        try {
            searchSmartPosts();
        } catch (IOException | ParserConfigurationException | SAXException ex) {
            Logger.getLogger(SmartPosts.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static SmartPosts getInstance(){
        if (sp==null)
            sp= new SmartPosts();
        return sp;
    }
    
    private void searchSmartPosts() throws MalformedURLException, IOException, ParserConfigurationException, SAXException{ //reads smartpost offices from open data
        URL url = new URL("http://smartpost.ee/fi_apt.xml");
        InputStream stream = url.openStream();
        
        String text;
        String content = "";
        
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(stream);
        doc.getDocumentElement().normalize();
        NodeList nodes = doc.getElementsByTagName("place");
        for (int i = 0; i < nodes.getLength() ; i++){
            Node node = nodes.item(i);
            Element element = (Element) node;
            String city = element.getElementsByTagName("city").item(0).getTextContent().toUpperCase();
            String office = element.getElementsByTagName("postoffice").item(0).getTextContent();
            String address = element.getElementsByTagName("address").item(0).getTextContent();
            String postNumber = element.getElementsByTagName("code").item(0).getTextContent();
            String lat = element.getElementsByTagName("lat").item(0).getTextContent();
            String lng = element.getElementsByTagName("lng").item(0).getTextContent();
            String hours = element.getElementsByTagName("availability").item(0).getTextContent();
            posts.add(new SmartPost(city, office, address, postNumber, lat, lng, hours));
            if (!cities.contains(city)){
                cities.add(city);
            }
        }
        Collections.sort(cities);
    }
    
    public ArrayList<String> getCities(){ //returns list of cities with smartposts offices
        return cities;
    }
    
    public ArrayList<SmartPost> getCitysSmartPosts(String name){ //return smartpost offices of certain city
        ArrayList<SmartPost> found = new ArrayList();
        for (SmartPost i : posts){
            if (i.getCity().equals(name))
                found.add(i);
        }
        return found;
    }
}
