/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyo;

import harjoitustyo.users.Users;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @author Pyry
 */
public class loginWindowController implements Initializable {
    
    private Label label;
    @FXML
    private TextField userField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Button loginButton;
    @FXML
    private Button createButton;
    @FXML
    private Label messageField;
    
    private Users u = Users.getInstance();
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void loginAction(ActionEvent event) { //signs in TIMO system if user name and password are correct else tells user that password or username are invalid
        if (u.checkUser(userField.getText(), passwordField.getText())) {
            try {
                Stage create = new Stage();
                Parent page = FXMLLoader.load(getClass().getResource("ui2.fxml"));
                Scene scene = new Scene(page);
                create.setScene(scene);
                create.show();
            } catch (IOException ex) {
                Logger.getLogger(loginWindowController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            messageField.setText("Käyttäjänimi tai salasana väärin.");
        }
    }

    @FXML
    private void createAccount(ActionEvent event) { // opens create Account window when create account button is clicked
        
        try {
            Stage create = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("createAccount.fxml"));
            Scene scene = new Scene(page);
            create.setScene(scene);
            create.show();
        } catch (IOException ex) {
            Logger.getLogger(loginWindowController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
