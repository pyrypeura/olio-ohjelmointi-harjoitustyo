package history;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class history {
    
    
       FileWriter writer = null;
       File h = new File("history.txt");
       private static history hi = null;
       
       private history(){
           
       }
       
       public static history getInstance (){
           if (hi == null)
               hi = new history();
           return hi;
       }
       

      public void eraseHistory() {
           try {
               writer = new FileWriter(h, false);
               writer.close();
           } catch (IOException ex) {
               Logger.getLogger(history.class.getName()).log(Level.SEVERE, null, ex);
           }
        
      }

      
      public void addHistory(String data) {
           try {
               Timestamp timestamp = new Timestamp(System.currentTimeMillis());
               String listItem = data+":"+timestamp+"\n";
               writer = new FileWriter(h, true);
               writer.append(listItem);
               writer.close();
               Scanner lukija = new Scanner(h);
               while (lukija.hasNextLine()){
                   System.out.println(lukija.nextLine());
               }
           } catch (IOException ex) {
               Logger.getLogger(history.class.getName()).log(Level.SEVERE, null, ex);
           }
      }

      public ArrayList<String> readHistory() {
        ArrayList<String> historyArray = null;
        try {
            historyArray = new ArrayList();
            Scanner s = new Scanner(h);
            while (s.hasNextLine()) {
                String line = s.nextLine();
                historyArray.add(line);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(history.class.getName()).log(Level.SEVERE, null, ex);
        }
        return historyArray;
    }


}
   