/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Packages;

import harjoitustyo.timo.SmartPost;
import java.util.Random;

/**
 *
 * @author Pyry
 */
public class Package3 extends Parcel {
    private final double maxSize=100000000.0;
    private final double maxWeight=10000.0;
    
    
    public Package3(Item i,SmartPost s, SmartPost e){
        super(i, s, e, 3);
    }
    
    @Override
    public boolean checkItem(){
        return true;
    }
    
    public void setItem(Item item){
        super.setItem(item);
    }
    
    @Override
    public void send(){
        Random randomGenerator = new Random();
        System.out.println(super.getItem());
        System.out.println(super.getItem().getWeight());
        double num = super.getItem().getWeight()*randomGenerator.nextFloat();
        Item item = super.getItem();
        
        if (item.isBreakable() && num>0.95){
            item.breakItem();
        }
    }
}
