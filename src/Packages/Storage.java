/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Packages;

import java.util.ArrayList;

/**
 *
 * @author Veeti
 */
public class Storage {
    private ArrayList<Parcel> packageList = new ArrayList();
    private static Storage s = null;
    
    private Storage(){
        
    }
    
    public static Storage getInstance(){
        if (s==null){
            s = new Storage();
        }
        return s;
    }
    
    public void addPackage(Parcel p){//adds package to storage
        packageList.add(p);
    }
    
    public ArrayList<Parcel> getList(){ //returns list of packages
        return packageList;
    }
}
