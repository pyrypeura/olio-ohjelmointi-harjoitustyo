/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Packages;

import harjoitustyo.timo.SmartPost;
import java.util.ArrayList;

/**
 *
 * @author Pyry
 */
public class Parcel {
    private Item item;
    private SmartPost start;
    private SmartPost end;
    private int classNumber;
    
    public Parcel(Item i,SmartPost s, SmartPost e, int cn){
        start = s;
        end = e;
        classNumber = cn;
        item = i;
    }
    
    public ArrayList<String> getRoute(){ //returns start and endpoint of packet as a arraylist
        ArrayList<String> route = new ArrayList();
        route.addAll(start.getGeoPoint().getList());
        route.addAll(end.getGeoPoint().getList());
        return route;
    }
    
    public void setItem(Item i) {
        item=i;
    }
    
    public Item getItem(){
        return item;
    }
    
    
    public void send(){
        
    }
    
    public boolean checkItem(){ //checks if item is fittable to parcel
        return true;
    }
    
    public int getClassNumber(){
        return classNumber;
    }
    
    public String getHistoryData(){ //returns data for historylog as string
        String broken = "Ei";
        if (item.isBroken())
            broken = "Kyllä";
        return "Esine: " + item.toString() + ", Lähtökaupunki: " + start.getCity() + ", Kohdekaupunki: " + end.getCity() + ", Rikkoutui: " + broken + ", Aika: " ;
    }
    
    public String toString(){
        return item.toString() + ", " + start.getCity() + " - " + end.getCity();
    }
}
