/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Packages;

import java.util.ArrayList;

/**
 *
 * @author Pyry
 */
public class ItemList {
    private ArrayList<Item> itemList = new ArrayList<>();
    private static ItemList il = null;
    
    public ItemList(){
        itemList.add(new Cake());
        itemList.add(new JellyJortikka());
        itemList.add(new Elephant());
        itemList.add(new gintonic());
    }
    
    public static ItemList getInstance(){
        if (il==null){
            il = new ItemList();
        }
        return il;
    }
    
    
    public ArrayList<Item> getList(){
        return itemList;
    }
    
    public void addItem(Item item){ //adds item to item list
        itemList.add(item);
    }
}
