/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Packages;

import harjoitustyo.timo.SmartPost;

/**
 *
 * @author Pyry
 */
public class Package1 extends Parcel {
    private final double maxSize= 500;
    private final double maxWeight=5;
    
    
    public Package1(Item i,SmartPost s, SmartPost e){
        super(i, s, e, 1);
    }
    
    @Override
    public boolean checkItem(){
        if (super.getItem().getSize()>maxSize || super.getItem().getWeight()>maxWeight)
            return false;
        return true;
    }
    
    public void addItem(Item item){
        super.setItem(item);
    }
    
    @Override
    public void send(){
        Item item = super.getItem();
        if (item.isBreakable()){
            item.breakItem();
        }
    }
}
